import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Team from "./components/Team/Team";
import Team2 from "./components/Team2/Team2";
import Team3 from "./components/Team3/Team3";
import Services from "./components/Services/Services";
import Testimonials from "./components/Testimonials/Testimonials";
import Testimonials2 from "./components/Testimonials2/Testimonials2";
import Blog from "./components/Blog/Blog";
import Contact from "./components/Contact/Contact";

const App = () => {
    return (
        <div className="main">
            <Header/>
            <Home/>
            <About/>
            <Team/>
            <Team2/>
            <Team3/>
            <Services/>
            <Testimonials/>
            <Testimonials2/>
            <Blog/>
            <Contact/>
        </div>
    );
};

export default App;

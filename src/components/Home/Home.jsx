import React, {Component} from 'react';
import classes from './Home.module.css';

class Home extends Component {

    render() {
        return(
            <div className={classes.section}>
                <div className={classes.container}>
                    <div className={classes.containerCenter}>
                        <div>
                            <h1>We Are The Best Consulting Agency</h1>
                        </div>
                        <div>
                            <h4>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam assumenda ea quo cupiditate facere deleniti fuga officia.</h4>
                        </div>
                        <div>
                            <button type="button"><h4>Get Started</h4></button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Home;
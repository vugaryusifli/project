import React, {Component} from 'react';
import person_1 from '../../images/person_1.jpg';
import person_2 from '../../images/person_2.jpg';
import person_3 from '../../images/person_3.jpg';
import person_4 from '../../images/person_4.jpg';
import person_5 from '../../images/person_5.jpg';
import person_6 from '../../images/person_6.jpg';
import person_7 from '../../images/person_7.jpg';
import person_8 from '../../images/person_8.jpg';
import classes from './Team.module.css';
import {FaFacebookF, FaTwitter, FaLinkedinIn, FaInstagram} from "react-icons/all";

class Team extends Component {

    render() {
        return(
            <div className={classes.section}>
                <div className={classes.container}>
                    <div className={classes.first}>
                        <h1>Our Dedicated Professionals</h1>
                        <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus minima neque tempora reiciendis.</h3>
                    </div>
                    <div className={classes.third}>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_1} alt=""/>
                            </div>
                            <div>
                                <h3>Kaiara Spencer</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_2} alt=""/>
                            </div>
                            <div>
                                <h3>Dave Simpson</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_3} alt=""/>
                            </div>
                            <div>
                                <h3>Ben Thompson</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_4} alt=""/>
                            </div>
                            <div>
                                <h3>Kyla Stewart</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_5} alt=""/>
                            </div>
                            <div>
                                <h3>Kaiara Spencer</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_6} alt=""/>
                            </div>
                            <div>
                                <h3>Dave Simpson</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_7} alt=""/>
                            </div>
                            <div>
                                <h3>Ben Thompson</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                        <div className={classes.member}>
                            <div className={classes.figure}>
                                <ul>
                                    <li>
                                        <div>
                                            <FaFacebookF/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaTwitter/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaLinkedinIn/>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <FaInstagram/>
                                        </div>
                                    </li>
                                </ul>
                                <img src={person_8} alt=""/>
                            </div>
                            <div>
                                <h3>Chris Stewart</h3>
                                <h4>Product Manager</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Team;
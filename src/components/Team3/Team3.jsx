import React, {Component} from 'react';
import classes from './Team3.module.css';

class Team3 extends Component {

    render() {
        return(
            <div className={classes.section}>
                <div className={classes.container}>
                    <div className={classes.containerTop}>
                        <h1>Our Approach</h1>
                    </div>
                    <div className={classes.containerBottom}>
                        <div className={classes.left}>
                            <h2><span>01.</span>Creative</h2>
                            <h4>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</h4>
                        </div>
                        <div className={classes.center}>
                            <h2><span>01.</span>Strategy</h2>
                            <h4>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</h4>
                        </div>
                        <div className={classes.right}>
                            <h2><span>01.</span>Production</h2>
                            <h4>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Team3;
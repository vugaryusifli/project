import React, {Component} from 'react';
import classes from './About.module.css';
import img1 from "../../images/img-1.jpg";

class About extends Component {

    render() {
        return(
            <div className={classes.section}>
                <div className={classes.container}>
                    <img src={img1} alt=""/>
                    <div className={classes.containerBox}>
                        <div className={classes.first}>
                            <h5>Creative skills</h5>
                        </div>
                        <div className={classes.second}>
                            <h1>Create Your Own Web Masterpiece</h1>
                        </div>
                        <div className={classes.third}>
                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis eveniet, voluptatem harum provident iusto modi explicabo! Aperiam velit reiciendis, eius impedit ea necessitatibus facilis nobis ipsum, architecto cum, doloribus nesciunt.</h4>
                        </div>
                        <div className={classes.fourth}>
                            <button type="button"><h4>Read More</h4></button>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}

export default About;
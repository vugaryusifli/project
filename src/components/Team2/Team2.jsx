import React, {Component} from 'react';
import classes from './Team2.module.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import slide_1 from '../../images/slide_1.jpg'
import slide_2 from '../../images/slide_2.jpg'
import slide_3 from '../../images/slide_3.jpg'
import slide_4 from '../../images/slide_4.jpg'

class Team2 extends Component {

    render() {
        return(
            <div className={classes.section}>
                <div className={classes.container}>
                    <div className={classes.containerLeft}>
                        <Carousel transitionTime={500} showThumbs={false} showIndicators={false} showStatus={false} infiniteLoop={true} emulateTouch={true}>
                            <div>
                                <img src={slide_1} alt=""/>
                            </div>
                            <div>
                                <img src={slide_2} alt=""/>
                            </div>
                            <div>
                                <img src={slide_3} alt=""/>
                            </div>
                            <div>
                                <img src={slide_4} alt=""/>
                            </div>
                        </Carousel>
                    </div>
                    <div className={classes.containerRight}>
                        <div className={classes.first}>
                            <h1>We Are The Best Consulting Agency</h1>
                        </div>
                        <div className={classes.second}>
                            <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h3>
                        </div>
                        <div className={classes.third}>
                            <h4>Est qui eos quasi ratione nostrum excepturi id recusandae fugit omnis ullam pariatur itaque nisi voluptas impedit Quo suscipit omnis iste velit maxime.</h4>
                        </div>
                        <div className={classes.fourth}>
                            <button type="button"><h4>Learn More</h4></button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Team2;
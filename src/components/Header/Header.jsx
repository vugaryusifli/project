import React, {Component} from 'react';
import classes from './Header.module.css';

class Header extends Component {

    render() {
        return(
            <div className={classes.header}>
                <div className={classes.leftMenu}>
                    <h2>Pointer<span className={classes.color}>.</span></h2>
                </div>
                <div className={`${classes.rightMenu} ${classes.items}`}>
                    <div className={`${classes.item} ${classes.active}`}><h4>Home</h4></div>
                    <div className={classes.item}><h4>About Us</h4></div>
                    <div className={classes.item}><h4>Team</h4></div>
                    <div className={classes.item}><h4>Services</h4></div>
                    <div className={classes.item}><h4>Testimonials</h4></div>
                    <div className={classes.item}><h4>Blog</h4></div>
                    <div className={classes.item}><h4>Contact</h4></div>
                </div>
            </div>
        );
    }

}

export default Header;